# Twilio-Synapse

A simple script that gets a new turn key from twilio and updates your homeserver.yaml file for Synapse.

## Deployment

1. Open the [config file](https://gitlab.com/Sleuth56/twilio-synapse/-/blob/master/config.yaml.default)
2. Put the path to your main homeserver.yaml file.
3. Put your twilio API keys in the config file.
4. Use systemd or cron to run the script automatically. Or run it manually up to you.
