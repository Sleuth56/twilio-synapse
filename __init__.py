# curl -X POST https://api.twilio.com/2010-04-01/Accounts/$TWILIO_ACCOUNT_SID/Tokens.json -u $TWILIO_ACCOUNT_SID:$TWILIO_AUTH_TOKEN

import yaml
import json
import requests
import os
from requests.auth import HTTPBasicAuth

yaml_contense = None
config_contense = None
endpoint = "https://api.twilio.com/2010-04-01/Accounts/{twilio_sid}/Tokens.json"

# open(os.path.join(__location__, 'config.yaml'))

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(os.path.join(__location__, 'config.yaml'), 'r') as config:
        config_contense = yaml.safe_load(config)

with open(config_contense['config_file'], 'r') as file:
    yaml_contense = yaml.safe_load(file)

    twilio_turn_stuff = requests.post(endpoint.format(twilio_sid=config_contense['twilio_sid']), auth=HTTPBasicAuth(config_contense['twilio_sid'], config_contense['auth_token'])).content
    twilio_turn_stuff = json.loads(twilio_turn_stuff.decode('utf-8'))

    yaml_contense['turn_username'] = twilio_turn_stuff['username']
    yaml_contense['turn_password'] = twilio_turn_stuff['password']

    # print(yaml_contense['turn_username'])
    # print(yaml_contense['turn_password'])


with open(config_contense['config_file'], 'w') as file:
    file.write(yaml.dump(yaml_contense))
